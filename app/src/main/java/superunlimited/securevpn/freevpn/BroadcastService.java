package superunlimited.securevpn.freevpn;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static superunlimited.securevpn.freevpn.Utils.isPremium;

import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.util.Timer;
import java.util.TimerTask;

import de.blinkt.openvpn.core.OpenVPNThread;

public class BroadcastService extends Service {

    private final static String TAG = "sssssssssss";

    public static final String COUNTDOWN_BR = "superunlimited.securevpn.freevpn.countdown_br";
    Intent bi = new Intent(COUNTDOWN_BR);
    private int time = 0;
    Timer t;


    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "Starting timer...");
        t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
                                  @RequiresApi(api = Build.VERSION_CODES.M)
                                  @Override
                                  public void run() {
                                      bi.putExtra("countdown", secondsToString(time));
                                      sendBroadcast(bi);
                                      time += 1;
                                      if (!isPremium) {
                                          if (secondsToString(time).equals("25:00")) {
                                              stopSelf();
                                              onDestroy();
                                              OpenVPNThread.stop();
                                              startActivity(new Intent(getBaseContext(), StartActivity.class).addFlags(FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).putExtra("premium", 1));
                                          }
                                      }

                                  }
                              },
                0,
                1000);


    }

    @Override
    public void onDestroy() {
        t.cancel();
        Log.i(TAG, "Timer cancelled");
        super.onDestroy();
    }

    private String secondsToString(int pTime) {
        final int min = pTime / 60;
        final int sec = pTime - (min * 60);

        final String strMin = placeZeroIfNeede(min);
        final String strSec = placeZeroIfNeede(sec);
        return String.format("%s:%s", strMin, strSec);
    }

    private String placeZeroIfNeede(int number) {
        return (number >= 10) ? Integer.toString(number) : String.format("0%s", number);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}