package superunlimited.securevpn.freevpn;

import static superunlimited.securevpn.freevpn.ads.InterstitialManager.admobinterstitialAd;

import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import superunlimited.securevpn.freevpn.ads.InterstitialManager;

public class CheckActivity extends AppCompatActivity {

    public boolean isActivityleft = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isActivityleft = false;
        new Handler().postDelayed(() -> {

            if (!Utils.getispremium(CheckActivity.this)) {
                if (admobinterstitialAd != null) {
                    InterstitialManager.ShowAdsaa(this, isActivityleft);
                } else {
                    InterstitialManager.LoadAdsaaa(CheckActivity.this);
                }
            }
            finish();

        }, 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActivityleft = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isActivityleft = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityleft = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityleft = false;

    }
}