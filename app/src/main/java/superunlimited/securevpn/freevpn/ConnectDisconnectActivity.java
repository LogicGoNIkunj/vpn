package superunlimited.securevpn.freevpn;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;

import superunlimited.securevpn.freevpn.ads.NativeManager;

public class ConnectDisconnectActivity extends AppCompatActivity {

    ImageView back, flag;
    TextView connectdisconnecttext, countryname, ipaddresss,duration,ping;
    LottieAnimationView anim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_disconnect);

        back = findViewById(R.id.back);
        back.setOnClickListener(v -> onBackPressed());

        flag = findViewById(R.id.countryflag);
        countryname = findViewById(R.id.countryname);
        duration = findViewById(R.id.duration);
        ping = findViewById(R.id.ping);
        connectdisconnecttext = findViewById(R.id.connectdisconnect);
        ipaddresss = findViewById(R.id.ipaddresss);
        anim = findViewById(R.id.anim);


        ipaddresss.setText(getIntent().getStringExtra("ipadress"));
        Glide.with(this).load(getIntent().getStringExtra("flag")).into(flag);
        countryname.setText(getIntent().getStringExtra("country"));
        duration.setText(getIntent().getStringExtra("duration"));
        ping.setText(getIntent().getStringExtra("ping"));

        if (getIntent().getBooleanExtra("condiscon", false)) {
            connectdisconnecttext.setText("CONNECTED");
            StartActivity.check = true;
            anim.setAnimation("connect.json");
            anim.loop(true);
            anim.playAnimation();
        } else {
            connectdisconnecttext.setText("DISCONNECTED");
            connectdisconnecttext.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            anim.setAnimation("disconnect.json");
            anim.loop(true);
            anim.playAnimation();
        }

        if (!Utils.getispremium(ConnectDisconnectActivity.this)) {
            RefreshAd();
        }
    }

    private void RefreshAd() {
        NativeManager.LoadNativeads(findViewById(R.id.admob_native_ll),this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}