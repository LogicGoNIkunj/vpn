package superunlimited.securevpn.freevpn;

import android.app.ProgressDialog;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import superunlimited.securevpn.freevpn.model.Feedback;


public class ContactUsActivity extends AppCompatActivity {
    TextView SendEmail;
    EditText TextDescription;
    EditText TextTitle;
    TextView DescriptionTitle;
    TextView textTitleText;
    ImageView imageViewBack;
    String version;
    private boolean isFatching = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        initUI();
        clickListener();
    }

    public void onBackPressed() {
        super.onBackPressed();
    }


    private void initUI() {

        this.textTitleText = findViewById(R.id.TitleText);
        this.DescriptionTitle = findViewById(R.id.DescriptionTitle);
        this.TextTitle = findViewById(R.id.TextTitle);
        this.TextDescription = findViewById(R.id.TextDescription);
        this.SendEmail = findViewById(R.id.SendEmail);
        imageViewBack = findViewById(R.id.imageViewBack);
        imageViewBack.setOnClickListener(v -> {

            onBackPressed();
        });
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void clickListener() {
        this.SendEmail.setOnClickListener(v -> {
            if (TextTitle.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(ContactUsActivity.this, getResources().getString(R.string.TITLE_TEXT_SET_PRE_CONDITION), Toast.LENGTH_SHORT).show();
            } else if (TextDescription.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(ContactUsActivity.this, getResources().getString(R.string.DESCRIPTION_TEXT_SET_PRE_CONDITION), Toast.LENGTH_SHORT).show();
            } else {
                PackageInfo packageInfo = null;
                try {
                    packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                String appverson = Objects.requireNonNull(packageInfo).versionName;
                String appname = getResources().getString(R.string.app_name);
                int appversoncode = Build.VERSION.SDK_INT;
                String mobilemodel = Build.MODEL;
                String packagename = getPackageName();
                PostFeedback(appverson, appname, String.valueOf(appversoncode), mobilemodel, packagename, TextTitle.getText().toString(), TextDescription.getText().toString());

            }
        });
    }

    private void PostFeedback(String appverson, String appname, String appversoncode, String mobilemodel, String packagename, String title, String description) {
        ProgressDialog progressBar = new ProgressDialog(this);
        progressBar.setMessage("Sending Feedback...");
        progressBar.show();
        if (!isFatching) {

            isFatching = true;
            VideoStatusServiceP videoStatusService = VideoStatusClient.getClient().create(VideoStatusServiceP.class);
            videoStatusService.sendfeedback(appname, packagename, title, description, mobilemodel, appversoncode, appverson).enqueue(new Callback<Feedback>() {
                @Override
                public void onResponse(@NonNull Call<Feedback> call, @NonNull Response<Feedback> response) {
                    isFatching = false;
                    if (Objects.requireNonNull(response.body()).getStatus()) {
                        Toast.makeText(ContactUsActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        TextDescription.setText("");
                        TextTitle.setText("");
                        progressBar.dismiss();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Feedback> call, @NonNull Throwable t) {
                    progressBar.dismiss();
                    isFatching = false;
                    Toast.makeText(ContactUsActivity.this, "Please send feedback in playstore", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


}
