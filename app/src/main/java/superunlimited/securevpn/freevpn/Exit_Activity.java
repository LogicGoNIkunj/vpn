package superunlimited.securevpn.freevpn;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import superunlimited.securevpn.freevpn.ads.NativeManager;

public class Exit_Activity extends AppCompatActivity {
    RatingBar rateStat;
    LinearLayout ll_star;
    int answerValue, height;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exit_activity);
        init();
        click();
        if (isNetworkAvailable())
            if (height > 1280)
                NativeManager.LoadNativeads(findViewById(R.id.admob_native_ll),this);
            else {
                findViewById(R.id.hgh).setVisibility(View.GONE);
            }
    }

    private void init() {
        getWindow().setBackgroundDrawable(null);
        sharedPreferences = getSharedPreferences("rateing", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        rateStat = findViewById(R.id.ratestar);
        ll_star = findViewById(R.id.ll_star);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        if (sharedPreferences.getBoolean("rates", false)) {
            ll_star.setVisibility(View.GONE);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void click() {
        findViewById(R.id.no).setOnClickListener(view -> finish());
        findViewById(R.id.exit).setOnClickListener(v -> {
            finish();
            finishAffinity();
        });
        rateStat.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            answerValue = (int) (rateStat.getRating());
            if (answerValue == 1) {
                feedback();
            } else if (answerValue == 2) {
                feedback();
            } else if (answerValue == 3) {
                feedback();
            } else if (answerValue == 4) {
                editor.putBoolean("rates", true);
                editor.apply();
                String str = "android.intent.action.VIEW";
                try {
                    startActivity(new Intent(str, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                } catch (ActivityNotFoundException unused) {
                    startActivity(new Intent(str, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                }
            } else if (answerValue == 5) {
                editor.putBoolean("rates", true);
                editor.apply();
                String str = "android.intent.action.VIEW";
                try {
                    startActivity(new Intent(str, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                } catch (ActivityNotFoundException unused) {
                    startActivity(new Intent(str, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                }
            } else {
                Toast.makeText(getApplicationContext(), "No Point", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getBoolean("rates", false)) {
            ll_star.setVisibility(View.GONE);
        }
    }

    public void feedback() {
        Intent intent1 = new Intent(this, ContactUsActivity.class);
        intent1.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent1);
    }
}