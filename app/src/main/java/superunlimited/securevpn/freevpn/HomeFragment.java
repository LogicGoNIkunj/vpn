package superunlimited.securevpn.freevpn;

import static android.app.Activity.RESULT_OK;
import static android.os.Looper.getMainLooper;
import static android.view.View.VISIBLE;
import static superunlimited.securevpn.freevpn.StartActivity.check;
import static superunlimited.securevpn.freevpn.StartActivity.count;
import static superunlimited.securevpn.freevpn.StartActivity.pager;
import static superunlimited.securevpn.freevpn.Utils.currenttime;
import static superunlimited.securevpn.freevpn.Utils.isPremium;
import static superunlimited.securevpn.freevpn.ads.InterstitialManager.admobinterstitialAd;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.VpnService;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.thekhaeng.pushdownanim.PushDownAnim;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

import de.blinkt.openvpn.OpenVpnApi;
import de.blinkt.openvpn.core.OpenVPNService;
import de.blinkt.openvpn.core.OpenVPNThread;
import de.blinkt.openvpn.core.VpnStatus;
import es.dmoral.toasty.Toasty;
import superunlimited.securevpn.freevpn.ads.InterstitialManager;
import superunlimited.securevpn.freevpn.ads.NativeManager;
import superunlimited.securevpn.freevpn.interfaces.Openlist;
import superunlimited.securevpn.freevpn.interfaces.StartServer;
import superunlimited.securevpn.freevpn.model.Server;

public class HomeFragment extends Fragment implements View.OnClickListener, StartServer {


    public static boolean refreshadapter = false;
    final Handler handler = new Handler();
    boolean vpnStart = false;
    Openlist openlist;
    Runnable runnable2;
    LinearLayout top2;
    ImageView countryflag, premium;
    LottieAnimationView lottie;
    private Server server;
    private CheckInternetConnection connection;
    private SharedPreference preference;
    private TextView tv_download, countryname, duration, download, upload, text, time;
    final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                setStatus(intent.getStringExtra("state"));
                OpenVPNService.setNotificationActivityClass(SplashActivity.class);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                String duration = intent.getStringExtra("duration");
                String lastPacketReceive = intent.getStringExtra("lastPacketReceive");
                String byteIn = intent.getStringExtra("byteIn");
                String byteOut = intent.getStringExtra("byteOut");

                if (duration == null) duration = "00:00:00";
                if (lastPacketReceive == null) lastPacketReceive = "0";
                if (byteIn == null) byteIn = "00.0 kb";
                if (byteOut == null) byteOut = "00.0 kb";
                updateConnectionStatus(duration, lastPacketReceive, byteIn, byteOut);




            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateGUI(intent);
        }
    };

    private void updateGUI(Intent intent) {
        if (intent.getExtras() != null) {
            String millisUntilFinished = intent.getStringExtra("countdown");
            duration.setText(millisUntilFinished);
            if (!isPremium) {
                if (millisUntilFinished.equals("25:00")) {
                    stopVpn();
                }
            }

        }
    }

    @Override
    public void onDestroy() {
        //  getActivity().stopService(new Intent(getContext(), BroadcastService.class));
        super.onDestroy();
    }

    public static HomeFragment newInstance(String s) {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        view.findViewById(R.id.select_country).setOnClickListener(this);
        top2 = view.findViewById(R.id.top2);
        countryname = view.findViewById(R.id.countryname);
        text = view.findViewById(R.id.text);
        time = view.findViewById(R.id.time);
        countryflag = view.findViewById(R.id.countryflag);
        premium = view.findViewById(R.id.premium);
        if (!Utils.getispremium(getContext())) {
            premium.setVisibility(VISIBLE);
            premium.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pager.setCurrentItem(2);
                }
            });
        } else {
            premium.setVisibility(View.GONE);
        }

        upload = view.findViewById(R.id.upload);
        download = view.findViewById(R.id.download);
        duration = view.findViewById(R.id.duration);
        initializeAll();
        OpenVPNService.setNotificationActivityClass(SplashActivity.class);
        if (requireActivity() instanceof StartActivity) {
            ((StartActivity) requireActivity()).setSendData(this);
        }
        return view;
    }

    private void initializeAll() {
        preference = new SharedPreference(requireContext());
        server = preference.getServer();
        updateCurrentServerIcon(server.getFlagUrl());

        connection = new CheckInternetConnection();


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tv_download = view.findViewById(R.id.tv_download);

        if (!Utils.getispremium(getContext())) {
            NativeManager.LoadNativeads(getActivity().findViewById(R.id.admob_native_ll), getActivity());
        }
        openlist = (Openlist) getContext();
        lottie = view.findViewById(R.id.home);
        isServiceRunning();
        VpnStatus.initLogCache(requireActivity().getCacheDir());


        PushDownAnim.setPushDownAnimTo(lottie).setScale(PushDownAnim.MODE_STATIC_DP, 10.0f).setDurationPush(50).setDurationRelease(125).setInterpolatorPush(PushDownAnim.DEFAULT_INTERPOLATOR).setInterpolatorRelease(PushDownAnim.DEFAULT_INTERPOLATOR).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utils.getispremium(getContext())) {
                    if (vpnStart) {
                        stopVpn();
                        requireActivity().startActivity(new Intent(requireActivity(), ConnectDisconnectActivity.class).putExtra("flag", server.getFlagUrl()).putExtra("country", server.getCountry()).putExtra("condiscon", false).putExtra("ipadress", server.getIpaddress()).putExtra("duration", duration.getText()).putExtra("ping", server.getTime()));
                        if (!Utils.getispremium(getContext())) {
                            if (admobinterstitialAd != null) {
                                InterstitialManager.ShowAdsaa(requireActivity(), false);
                            } else {
                                InterstitialManager.LoadAdsaaa(requireActivity());
                            }
                        }
                    } else {
                        lottie.playAnimation();
                        lottie.loop(true);
                        prepareVpn();
                    }

                } else {
                    SharedPreferences sharedPreference = getContext().getApplicationContext().getSharedPreferences("premium", Context.MODE_PRIVATE);
                    if (!sharedPreference.getBoolean("used", false)) {
                        if (vpnStart) {
                            stopVpn();
                            requireActivity().startActivity(new Intent(requireActivity(), ConnectDisconnectActivity.class).putExtra("flag", server.getFlagUrl()).putExtra("country", server.getCountry()).putExtra("condiscon", false).putExtra("ipadress", server.getIpaddress()).putExtra("duration", duration.getText()).putExtra("ping", server.getTime()));
                            if (!Utils.getispremium(getContext())) {
                                if (admobinterstitialAd != null) {
                                    InterstitialManager.ShowAdsaa(requireActivity(), false);
                                } else {
                                    InterstitialManager.LoadAdsaaa(requireActivity());
                                }
                            }
                        } else {

                            final View alertLayout = LayoutInflater.from(getContext()).inflate(R.layout.serverconnectdialog, null);
                            LinearLayout freelayout = alertLayout.findViewById(R.id.freelayout);
                            RelativeLayout paidlayout = alertLayout.findViewById(R.id.paidlayout);

                            if (server.getFlagUrl()!=null){
                                Glide.with(getActivity()).load(server.getFlagUrl()).into((ImageView) alertLayout.findViewById(R.id.logo));

                            }

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setView(alertLayout);
                            alert.setCancelable(true);
                            final AlertDialog dialog = alert.create();
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                            dialog.show();

                            freelayout.setOnClickListener(arg0 -> {

                                lottie.playAnimation();
                                lottie.loop(true);
                                prepareVpn();

                                if (dialog != null && dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            });
                            paidlayout.setOnClickListener(v1 -> {
                                pager.setCurrentItem(2);
                                if (dialog != null && dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            });


                        }
                    } else {
                        Toast.makeText(getActivity(), "Buy Premium to Unlimited Use", Toast.LENGTH_SHORT).show();
                        pager.setCurrentItem(2);
                    }
                }


            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.select_country:
                openlist.openvpnlist("s");
        }
    }

    private void prepareVpn() {
        if (!vpnStart) {
            if (getInternetStatus()) {
                Intent intent = VpnService.prepare(getContext());
                if (intent != null) {
                    startActivityForResult(intent, 1);
                } else startVpn();
                status("connecting");

            } else {
                showToast("you have no internet connection !!");
            }

        } else if (stopVpn()) {
            showToast("Disconnect Successfully");
        }
    }

    public boolean stopVpn() {
        try {
            OpenVPNThread.stop();
            status("connect");
            vpnStart = false;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            startVpn();
        } else {
            showToast("Permission Deny !! ");
        }
    }

    public boolean getInternetStatus() {
        return connection.netCheck(requireContext());
    }


    public void isServiceRunning() {
        setStatus(OpenVPNService.getStatus());
    }

    private void startVpn() {
        try {
            // .ovpn file
            if (new File(server.getOvpn()).exists()) {

                FileInputStream conf = new FileInputStream(server.getOvpn());
                InputStreamReader isr = new InputStreamReader(conf);
                BufferedReader br = new BufferedReader(isr);
                String config = "";
                String line;

                while (true) {
                    line = br.readLine();
                    if (line == null) break;
                    config += line + "\n";
                }
                br.readLine();
                OpenVpnApi.startVpn(getContext(), config, server.getCountry(), server.getOvpnUserName(), server.getOvpnUserPassword());
                vpnStart = true;
            }
        } catch (IOException | RemoteException e) {
            e.printStackTrace();
        }
    }

    public void setStatus(String connectionState) {
        if (connectionState != null)
            switch (connectionState) {
                case "DISCONNECTED":
                    status("connect");
                    vpnStart = false;
                    OpenVPNService.setDefaultStatus();
                    if (!requireActivity().isFinishing()) {
                        Toasty.error(requireActivity(), "Disconnect", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case "CONNECTED":
                    vpnStart = true;
                    status("connected");
                    lottie.setAnimation("connected.json");
                    currenttime = (int) System.currentTimeMillis();
                    getActivity().startService(new Intent(getActivity(), BroadcastService.class));

                    if (!requireActivity().isFinishing()) {
                        Toasty.success(requireActivity(), "Connected Successfully", Toast.LENGTH_SHORT, true).show();
                    }
                    break;
                case "RECONNECTING":
                    status("connecting");
                    break;

            }

    }

    public void onResetClicked() {
        tv_download.setText("CONNECT");
        tv_download.setTextColor(getResources().getColor(R.color.black));
        getActivity().stopService(new Intent(getContext(), BroadcastService.class));
    }

    public void status(String status) {
        handler.removeCallbacks(runnable2);
        if (status.equals("connect")) {

            lottie.setAnimation("connectt.json");
            lottie.playAnimation();
            lottie.loop(true);

            handler.removeCallbacks(runnable2);
            tv_download.setText(requireContext().getString(R.string.connect));
            tv_download.setTextColor(getResources().getColor(R.color.black));
            top2.setVisibility(View.GONE);
            lottie.addAnimatorListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    lottie.setProgress(0);
                }
            });
            lottie.clearAnimation();
            lottie.cancelAnimation();
            lottie.setProgress(0);
            onResetClicked();
        } else if (status.equals("connecting")) {
            tv_download.setText(requireContext().getString(R.string.connecting));
            tv_download.setTextColor(getResources().getColor(R.color.black));
            top2.setVisibility(View.GONE);

            runnable2 = new Runnable() {
                int count = 0;

                @Override
                public void run() {
                    count++;

                    if (count == 1) {
                        tv_download.setText("Connecting.");
                    } else if (count == 2) {
                        tv_download.setText("Connecting..");
                    } else if (count == 3) {
                        tv_download.setText("Connecting...");
                    }

                    if (count == 3)
                        count = 0;

                    handler.postDelayed(this, 2 * 500);
                }
            };
            handler.postDelayed(runnable2, 500);

        } else if (status.equals("connected")) {
            handler.removeCallbacks(runnable2);
            top2.setVisibility(VISIBLE);
            tv_download.setText(requireContext().getString(R.string.disconnect));
            tv_download.setTextColor(getResources().getColor(R.color.green));
            if (!Utils.getispremium(getContext())) {
                if (admobinterstitialAd != null) {
                    InterstitialManager.ShowAdsaa(requireActivity(), false);
                } else {
                    InterstitialManager.LoadAdsaaa(requireActivity());
                }
            }
            SharedPreferences sharedPreference = getContext().getSharedPreferences("premium", Context.MODE_PRIVATE);

            if (sharedPreference.getFloat("data",0.0f)!=0.0f){

                SharedPreferences.Editor editor = sharedPreference.edit();
                editor.putFloat("ccc",(sharedPreference.getFloat("data",0.0f)+sharedPreference.getFloat("ccc",0.0f))).commit();
            }

        } else if (status.equals("tryDifferentServer")) {
            tv_download.setText("Try Different\nServer");
            tv_download.setTextColor(getResources().getColor(R.color.black));
        } else if (status.equals("loading")) {
            tv_download.setText("Loading Server..");
            tv_download.setTextColor(getResources().getColor(R.color.black));
        } else if (status.equals("invalidDevice")) {
            tv_download.setText("Invalid Device");
            tv_download.setTextColor(getResources().getColor(R.color.black));
        } else if (status.equals("authenticationCheck")) {
            tv_download.setText("Authentication \n Checking...");
            tv_download.setTextColor(getResources().getColor(R.color.black));
        }

    }

    public void updateConnectionStatus(String durations, String lastPacketReceive, String byteIn, String byteOut) {
        download.setText(byteIn);
        upload.setText(byteOut);
    }

    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void updateCurrentServerIcon(String serverIcon) {
        Glide.with(requireActivity()).load(serverIcon).into(countryflag);
        countryname.setText(server.getCountry());
        text.setText(server.getText());
        time.setText(server.getTime());
    }

    @Override
    public void StartnewServer(Server server) {

        try {
            if (vpnStart) {
                stopVpn();
            }
            HomeFragment.this.server = server;
            updateCurrentServerIcon(server.getFlagUrl());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        count++;

        if (server == null) {
            server = preference.getServer();
        }
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(broadcastReceiver, new IntentFilter("connectionState"));
        getActivity().registerReceiver(br, new IntentFilter(BroadcastService.COUNTDOWN_BR));
        if (check) {
            check = false;
            if (!SharePreferenceUtil.isRated(requireActivity())) {
                try {
                    new RateDialog(requireActivity(), false).show();
                    check = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        SharedPreferences sharedPreference = getContext().getSharedPreferences("premium", Context.MODE_PRIVATE);
        if (sharedPreference.getBoolean("used",false)){
            status("connect");
            vpnStart = false;
        }

    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(requireActivity()).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    public void onStop() {
        if (server != null) {
            preference.saveServer(server);
        }
        super.onStop();
    }


}
