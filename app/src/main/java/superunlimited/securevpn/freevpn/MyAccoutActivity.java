package superunlimited.securevpn.freevpn;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAccoutActivity extends AppCompatActivity {
    TextView connectiontype,expdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_accout);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        connectiontype = findViewById(R.id.connectiontype);
        expdate = findViewById(R.id.expdate);
        expdate.setText(Utils.subscribe_time );

        if (Utils.getispremium(MyAccoutActivity.this)) {
            connectiontype.setText("Premium");
        } else {
            connectiontype.setText("Free");
        }

    }
}