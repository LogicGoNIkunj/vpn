package superunlimited.securevpn.freevpn;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import superunlimited.securevpn.freevpn.ads.InterstitialManager;
import superunlimited.securevpn.freevpn.ads.openappads.AppOpenManager;

public class MyApplication extends Application {
    public static String MYSECRET;
    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;
    public static AppOpenManager appOpenManager;
    public static Context context;


    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        appOpenManager = new AppOpenManager(this);
        MyApplication.appOpenManager.isShowingAd = true;
        sharedPreferences = getSharedPreferences("ps", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        if (!Utils.getispremium(context)) {
            InterstitialManager.LoadAdsaaa(this);
        }

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "superunlimited.securevpn.freevpn", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String key = Base64.encodeToString(md.digest(), Base64.NO_WRAP);
                key = key.replaceAll("[^a-zA-Z0-9]", "");
                MYSECRET = "abcdefg";
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    //admob banner
    public static void set_Admob_banner_Id(String Admob_banner_Id) {
        editor.putString("Admob_banner_Id", Admob_banner_Id).commit();
    }

    public static String get_Admob_banner_Id() {
        return sharedPreferences.getString("Admob_banner_Id", context.getString(R.string.banner));
    }

    //admob interstitial
    public static void set_Admob_interstitial_Id(String Admob_interstitial_Id) {
        editor.putString("Admob_interstitial_Id", Admob_interstitial_Id).commit();
    }

    public static String get_Admob_interstitial_Id() {
        return sharedPreferences.getString("Admob_interstitial_Id", context.getString(R.string.splash_interestial_add));
    }

    //admob open
    public static void set_Admob_open_Id(String Admob_open_Id) {
        editor.putString("Admob_open_Id", Admob_open_Id).commit();
    }

    public static String get_Admob_open_Id() {
        return sharedPreferences.getString("Admob_open_Id", "");
    }

    //admob native
    public static void set_Admob_native_Id(String Admob_native_Id) {
        editor.putString("Admob_native_Id", Admob_native_Id).commit();
    }

    public static String get_Admob_native_Id() {
        return sharedPreferences.getString("Admob_native_Id", context.getString(R.string.nativead_id));
    }

    //admob native
    public static void set_Admob_smallnative_Id(String Admob_smallnative_Id) {
        editor.putString("Admob_smallnative_Id", Admob_smallnative_Id).commit();
    }

    public static String get_Admob_smallnative_Id() {
        return sharedPreferences.getString("Admob_smallnative_Id", context.getString(R.string.nativead_id));
    }

}
