package superunlimited.securevpn.freevpn;

import static superunlimited.securevpn.freevpn.Utils.currency;
import static superunlimited.securevpn.freevpn.Utils.monthly;
import static superunlimited.securevpn.freevpn.Utils.monthlykey;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.PurchaseInfo;

public class PremiumActivity extends AppCompatActivity implements BillingProcessor.IBillingHandler{
    private BillingProcessor bp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium);

        bp = new BillingProcessor(this, getResources().getString(R.string.play_console_license), this);
        bp.initialize();

        findViewById(R.id.cancel_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ((TextView) findViewById(R.id.start)).setText("Start with "+currency+" "+monthly+"/month");

        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bp.isSubscriptionUpdateSupported()) {
                    bp.subscribe(PremiumActivity.this, monthlykey);
                } else {
                    Log.d("MainActivity", "onBillingInitialized: Subscription updated is not supported");
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(PremiumActivity.this, StartActivity.class));
        finish();
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable PurchaseInfo details) {
        Utils.setispremium(PremiumActivity.this, true);
        startActivity(new Intent(PremiumActivity.this, StartActivity.class));
        finish();
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }
    @Override
    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }
}