package superunlimited.securevpn.freevpn;

import static superunlimited.securevpn.freevpn.StartActivity.count;
import static superunlimited.securevpn.freevpn.StartActivity.pager;
import static superunlimited.securevpn.freevpn.Utils.currency;
import static superunlimited.securevpn.freevpn.Utils.halfyear;
import static superunlimited.securevpn.freevpn.Utils.halfyear_price;
import static superunlimited.securevpn.freevpn.Utils.halfyearkey;
import static superunlimited.securevpn.freevpn.Utils.isPremium;
import static superunlimited.securevpn.freevpn.Utils.monthly;
import static superunlimited.securevpn.freevpn.Utils.monthly_price;
import static superunlimited.securevpn.freevpn.Utils.monthlykey;
import static superunlimited.securevpn.freevpn.Utils.yearly;
import static superunlimited.securevpn.freevpn.Utils.yearly_price;
import static superunlimited.securevpn.freevpn.Utils.yearlykey;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.PurchaseInfo;

public class PremiumFragment extends Fragment implements BillingProcessor.IBillingHandler {
    private BillingProcessor bp;
    private ImageView back;
    private ImageView btnPremium;

    TextView price, pricedrop, txtmonthprice, txt1month;
    TextView price6month, pricedrop6month, txt6monthprice, txt6month;
    TextView price12month, pricedrop12month, txt12monthprice, txt12month;
    ImageView arr, arrb, arrc;
    RelativeLayout m1, m2, m3;
    String premium;

    public static PremiumFragment newInstance(String s) {
        return new PremiumFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_premium, container, false);


        btnPremium = view.findViewById(R.id.btn_premium);
        back = view.findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        bp = new BillingProcessor(getContext(), getResources().getString(R.string.play_console_license), this);
        bp.initialize();

        btnPremium.setOnClickListener(v -> {
            if (bp.isSubscriptionUpdateSupported()) {
                bp.subscribe(getActivity(), premium);
            } else {
                Log.d("MainActivity", "onBillingInitialized: Subscription updated is not supported");
            }
        });

        price = (TextView) view.findViewById(R.id.price);
        pricedrop = (TextView) view.findViewById(R.id.pricedrop);
        txtmonthprice = (TextView) view.findViewById(R.id.txtmonthprice);
        txt1month = (TextView) view.findViewById(R.id.txt1month);

        price6month = (TextView) view.findViewById(R.id.price6month);
        pricedrop6month = (TextView) view.findViewById(R.id.pricedrop6month);
        txt6monthprice = (TextView) view.findViewById(R.id.txt6monthprice);
        txt6month = (TextView) view.findViewById(R.id.txt6month);

        price12month = (TextView) view.findViewById(R.id.price12month);
        pricedrop12month = (TextView) view.findViewById(R.id.pricedrop12month);
        txt12monthprice = (TextView) view.findViewById(R.id.txt12monthprice);
        txt12month = (TextView) view.findViewById(R.id.txt12month);

        arr = view.findViewById(R.id.arr);
        arrb = view.findViewById(R.id.arrb);
        arrc = view.findViewById(R.id.arrc);

        m1 = view.findViewById(R.id.m1);
        m2 = view.findViewById(R.id.m2);
        m3 = view.findViewById(R.id.m3);




        price.setTextColor(getResources().getColor(R.color.white));
        pricedrop.setTextColor(getResources().getColor(R.color.white));
        txtmonthprice.setTextColor(getResources().getColor(R.color.white));
        txt1month.setTextColor(getResources().getColor(R.color.white));

        price6month.setTextColor(getResources().getColor(R.color.black));
        pricedrop6month.setTextColor(getResources().getColor(R.color.black));
        txt6monthprice.setTextColor(getResources().getColor(R.color.black));
        txt6month.setTextColor(getResources().getColor(R.color.black));

        price12month.setTextColor(getResources().getColor(R.color.black));
        pricedrop12month.setTextColor(getResources().getColor(R.color.black));
        txt12monthprice.setTextColor(getResources().getColor(R.color.black));
        txt12month.setTextColor(getResources().getColor(R.color.black));

        arr.setImageDrawable(getResources().getDrawable(R.drawable.click_arrow_01));
        arrb.setImageDrawable(getResources().getDrawable(R.drawable.click_arrow_black_01));
        arrc.setImageDrawable(getResources().getDrawable(R.drawable.click_arrow_black_01));

        m1.setBackgroundDrawable(getResources().getDrawable(R.drawable.selected_bg_01));
        m2.setBackgroundDrawable(getResources().getDrawable(R.drawable.rec_01));
        m3.setBackgroundDrawable(getResources().getDrawable(R.drawable.rec_01));

        premium = monthlykey;

        m1.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                price.setTextColor(getResources().getColor(R.color.white));
                pricedrop.setTextColor(getResources().getColor(R.color.white));
                txtmonthprice.setTextColor(getResources().getColor(R.color.white));
                txt1month.setTextColor(getResources().getColor(R.color.white));

                price6month.setTextColor(getResources().getColor(R.color.black));
                pricedrop6month.setTextColor(getResources().getColor(R.color.black));
                txt6monthprice.setTextColor(getResources().getColor(R.color.black));
                txt6month.setTextColor(getResources().getColor(R.color.black));

                price12month.setTextColor(getResources().getColor(R.color.black));
                pricedrop12month.setTextColor(getResources().getColor(R.color.black));
                txt12monthprice.setTextColor(getResources().getColor(R.color.black));
                txt12month.setTextColor(getResources().getColor(R.color.black));

                arr.setImageDrawable(getResources().getDrawable(R.drawable.click_arrow_01));
                arrb.setImageDrawable(getResources().getDrawable(R.drawable.click_arrow_black_01));
                arrc.setImageDrawable(getResources().getDrawable(R.drawable.click_arrow_black_01));

                m1.setBackgroundDrawable(getResources().getDrawable(R.drawable.selected_bg_01));
                m2.setBackgroundDrawable(getResources().getDrawable(R.drawable.rec_01));
                m3.setBackgroundDrawable(getResources().getDrawable(R.drawable.rec_01));

                premium = monthlykey;
                if (bp.isSubscriptionUpdateSupported()) {
                    bp.subscribe(getActivity(), premium);
                } else {
                    Log.d("MainActivity", "onBillingInitialized: Subscription updated is not supported");
                }
            }
        });


        m2.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                price.setTextColor(getResources().getColor(R.color.black));
                pricedrop.setTextColor(getResources().getColor(R.color.black));
                txtmonthprice.setTextColor(getResources().getColor(R.color.black));
                txt1month.setTextColor(getResources().getColor(R.color.black));

                price6month.setTextColor(getResources().getColor(R.color.white));
                pricedrop6month.setTextColor(getResources().getColor(R.color.white));
                txt6monthprice.setTextColor(getResources().getColor(R.color.white));
                txt6month.setTextColor(getResources().getColor(R.color.white));

                price12month.setTextColor(getResources().getColor(R.color.black));
                pricedrop12month.setTextColor(getResources().getColor(R.color.black));
                txt12monthprice.setTextColor(getResources().getColor(R.color.black));
                txt12month.setTextColor(getResources().getColor(R.color.black));

                arr.setImageDrawable(getResources().getDrawable(R.drawable.click_arrow_black_01));
                arrb.setImageDrawable(getResources().getDrawable(R.drawable.click_arrow_01));
                arrc.setImageDrawable(getResources().getDrawable(R.drawable.click_arrow_black_01));

                m1.setBackgroundDrawable(getResources().getDrawable(R.drawable.rec_01));
                m2.setBackgroundDrawable(getResources().getDrawable(R.drawable.selected_bg_01));
                m3.setBackgroundDrawable(getResources().getDrawable(R.drawable.rec_01));

                premium = halfyearkey;
                if (bp.isSubscriptionUpdateSupported()) {
                    bp.subscribe(getActivity(), premium);
                } else {
                    Log.d("MainActivity", "onBillingInitialized: Subscription updated is not supported");
                }
            }
        });

        m3.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                price.setTextColor(getResources().getColor(R.color.black));
                pricedrop.setTextColor(getResources().getColor(R.color.black));
                txtmonthprice.setTextColor(getResources().getColor(R.color.black));
                txt1month.setTextColor(getResources().getColor(R.color.black));

                price6month.setTextColor(getResources().getColor(R.color.black));
                pricedrop6month.setTextColor(getResources().getColor(R.color.black));
                txt6monthprice.setTextColor(getResources().getColor(R.color.black));
                txt6month.setTextColor(getResources().getColor(R.color.black));

                price12month.setTextColor(getResources().getColor(R.color.white));
                pricedrop12month.setTextColor(getResources().getColor(R.color.white));
                txt12monthprice.setTextColor(getResources().getColor(R.color.white));
                txt12month.setTextColor(getResources().getColor(R.color.white));

                arr.setImageDrawable(getResources().getDrawable(R.drawable.click_arrow_black_01));
                arrb.setImageDrawable(getResources().getDrawable(R.drawable.click_arrow_black_01));
                arrc.setImageDrawable(getResources().getDrawable(R.drawable.click_arrow_01));

                m1.setBackgroundDrawable(getResources().getDrawable(R.drawable.rec_01));
                m2.setBackgroundDrawable(getResources().getDrawable(R.drawable.rec_01));
                m3.setBackgroundDrawable(getResources().getDrawable(R.drawable.selected_bg_01));

                premium = yearlykey;
                if (bp.isSubscriptionUpdateSupported()) {
                    bp.subscribe(getActivity(), premium);
                } else {
                    Log.d("MainActivity", "onBillingInitialized: Subscription updated is not supported");
                }
            }
        });


        pricedrop.setPaintFlags(pricedrop.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        pricedrop6month.setPaintFlags(pricedrop6month.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        pricedrop12month.setPaintFlags(pricedrop12month.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


        return view;
    }

    @Override
    public void onBillingInitialized() {

    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable PurchaseInfo details) {
        Log.d("MainActivity", "onProductPurchased: ");
        Utils.setispremium(getContext(), true);
        startActivity(new Intent(getContext(), StartActivity.class));
        getActivity().finish();
    }

    @Override
    public void onPurchaseHistoryRestored() {
        Log.d("MainActivity", "onPurchaseHistoryRestored: ");

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        Log.d("MainActivity", "onBillingError: ");

    }

    @Override
    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        count++;
        price.setText(currency+" " + monthly );
        price6month.setText(currency+" " + halfyear);
        price12month.setText(currency+" " + yearly);
        pricedrop.setText(currency+" " + monthly_price );
        pricedrop6month.setText(currency+" " + halfyear_price);
        pricedrop12month.setText(currency+" " + yearly_price);
        try {
            txtmonthprice.setText(currency+" " + String.format("%.02f", Float.parseFloat(monthly)) + "/month");
            txt6monthprice.setText(currency+" " + String.format("%.02f", Float.parseFloat(halfyear) / 6 )+ "/month");
            txt12monthprice.setText(currency+" " + String.format("%.02f", Float.parseFloat(yearly) / 12 )+ "/month");
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
