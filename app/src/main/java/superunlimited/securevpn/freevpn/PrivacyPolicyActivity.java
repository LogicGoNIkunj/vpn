package superunlimited.securevpn.freevpn;

import static superunlimited.securevpn.freevpn.MyApplication.editor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

public class PrivacyPolicyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        SharedPreferences sharedPreference = getSharedPreferences("splash", Context.MODE_PRIVATE);
        editor = sharedPreference.edit();
        ((WebView)findViewById(R.id.policyurl)).loadUrl("https://sites.google.com/view/learningtosmart/home");
        ((TextView)findViewById(R.id.agree)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PrivacyPolicyActivity.this,StartActivity.class));
                finish();
            }
        });
        ((ImageView)findViewById(R.id.close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}