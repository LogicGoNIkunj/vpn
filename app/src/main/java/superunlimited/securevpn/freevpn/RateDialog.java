package superunlimited.securevpn.freevpn;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class RateDialog extends Dialog implements View.OnClickListener {
    private final Activity context;
    private final boolean exit;
    private final ImageView[] imageViewStars = new ImageView[5];
    private int star_number;

    public RateDialog(@NonNull Activity activity, boolean z) {
        super(activity, android.R.style.Theme_Material_Dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getWindow().setAttributes(lp);

        this.context = activity;
        this.exit = z;
        setContentView(R.layout.my_dialog_smart_rate);
        initView();
        this.star_number = 0;
    }

    private void initView() {
        TextView tvexit = findViewById(R.id.tv_exit);
        ViewGroup ratingBar = findViewById(R.id.scaleRatingBar);
        tvexit.setOnClickListener(this);
        ImageView iv_star_1 = findViewById(R.id.star_1);
        ImageView iv_star_2 = findViewById(R.id.star_2);
        ImageView iv_star_3 = findViewById(R.id.star_3);
        ImageView iv_star_4 = findViewById(R.id.star_4);
        ImageView iv_star_5 = findViewById(R.id.star_5);
        iv_star_1.setOnClickListener(this);
        iv_star_2.setOnClickListener(this);
        iv_star_3.setOnClickListener(this);
        iv_star_4.setOnClickListener(this);
        iv_star_5.setOnClickListener(this);
        this.imageViewStars[0] = iv_star_1;
        this.imageViewStars[1] = iv_star_2;
        this.imageViewStars[2] = iv_star_3;
        this.imageViewStars[3] = iv_star_4;
        this.imageViewStars[4] = iv_star_5;
        for (int i = 0; i < this.imageViewStars.length; i++) {
            if (i < 4) {
                this.imageViewStars[i].setImageResource(R.drawable.ic_star);
            } else {
                this.imageViewStars[i].setImageResource(R.drawable.ic_star_blur);
            }
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id != R.id.tv_exit) {
            switch (id) {
                case R.id.star_1:
                    this.star_number = 1;
                    setStarBar();
                    return;
                case R.id.star_2:
                    this.star_number = 2;
                    setStarBar();
                    return;
                case R.id.star_3:
                    this.star_number = 3;
                    setStarBar();
                    return;
                case R.id.star_4:
                    this.star_number = 4;
                    setStarBar();
                    return;
                case R.id.star_5:
                    this.star_number = 5;
                    setStarBar();
                    return;
                default:

            }
            if (this.star_number >= 0) {
                this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
                SharePreferenceUtil.setRated(this.context, true);
                dismiss();

            }
        } else if (this.exit) {

        } else {
            dismiss();

        }
    }

    private void setStarBar() {
        for (int i = 0; i < this.imageViewStars.length; i++) {
            if (i < this.star_number) {
                this.imageViewStars[i].setImageResource(R.drawable.ic_star);
            } else {
                this.imageViewStars[i].setImageResource(R.drawable.ic_star_blur);
            }
        }

        if (star_number == 1) {
            this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
            SharePreferenceUtil.setRated(this.context, true);
            dismiss();

        } else if (star_number == 2) {
            this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
            SharePreferenceUtil.setRated(this.context, true);
            dismiss();

        } else if (star_number == 3) {
            this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
            SharePreferenceUtil.setRated(this.context, true);
            dismiss();

        } else if (star_number == 4) {
            this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
            SharePreferenceUtil.setRated(this.context, true);
            dismiss();

        } else {
            this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
            SharePreferenceUtil.setRated(this.context, true);
            dismiss();

        }

    }
}
