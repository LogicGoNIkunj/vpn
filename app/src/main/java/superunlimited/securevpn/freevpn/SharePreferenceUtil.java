package superunlimited.securevpn.freevpn;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePreferenceUtil {
    private static final String RATE_COUNTER = "RATE_COUNTER";

    public static boolean isRated(Context context) {
        return context.getSharedPreferences(RATE_COUNTER, 0).getBoolean("is_rated_2", false);
    }

    public static void setRated(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences(RATE_COUNTER, 0).edit();
        edit.putBoolean("is_rated_2", z);
        edit.apply();
    }
}
