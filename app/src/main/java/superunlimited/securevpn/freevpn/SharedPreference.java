package superunlimited.securevpn.freevpn;

import android.content.Context;
import android.content.SharedPreferences;

import superunlimited.securevpn.freevpn.model.Server;

public class SharedPreference {

    private static final String APP_PREFS_NAME = "CakeVPNPreference";
    private static final String SERVER_COUNTRY = "server_country";
    private static final String SERVER_FLAG = "server_flag";
    private static final String SERVER_ID = "server_id";
    private static final String SERVER_OVPN = "server_ovpn";
    private static final String SERVER_TIME = "server_time";
    private static final String SERVER_TEXT = "server_text";
    private static final String SERVER_IS_PREMIUM = "server_is_premium";
    private static final String SERVER_OVPN_USER = "server_ovpn_user";
    private static final String SERVER_OVPN_PASSWORD = "server_ovpn_password";
    private static final String SERVER_IP_ADDRESS = "server_ovpn_ipaddress";
    private SharedPreferences mPreference;
    private SharedPreferences.Editor mPrefEditor;

    public SharedPreference(Context context) {
        this.mPreference = context.getSharedPreferences(APP_PREFS_NAME, Context.MODE_PRIVATE);
        this.mPrefEditor = mPreference.edit();
    }

    public void saveServer(Server server) {
        mPrefEditor.putString(SERVER_COUNTRY, server.getCountry());
        mPrefEditor.putString(SERVER_FLAG, server.getFlagUrl());
        mPrefEditor.putString(SERVER_OVPN, server.getOvpn());
        mPrefEditor.putString(SERVER_ID, server.getID());
        mPrefEditor.putString(SERVER_OVPN_USER, server.getOvpnUserName());
        mPrefEditor.putString(SERVER_OVPN_PASSWORD, server.getOvpnUserPassword());
        mPrefEditor.putString(SERVER_IP_ADDRESS, server.getIpaddress());
        mPrefEditor.putString(SERVER_TIME, server.getTime());
        mPrefEditor.putString(SERVER_TEXT, server.getText());
        mPrefEditor.putString(SERVER_IS_PREMIUM, server.getIs_premium()     );
        mPrefEditor.commit();
    }

    public Server getServer() {

        Server server = new Server(
                mPreference.getString(SERVER_COUNTRY, ""),
                mPreference.getString(SERVER_ID, ""),
                mPreference.getString(SERVER_FLAG, ""),
                mPreference.getString(SERVER_OVPN, ""),
                mPreference.getString(SERVER_OVPN_USER, ""),
                mPreference.getString(SERVER_OVPN_PASSWORD, ""),
                mPreference.getString(SERVER_IP_ADDRESS, ""),
                mPreference.getString(SERVER_TIME, ""),
                mPreference.getString(SERVER_TEXT, ""),
                mPreference.getString(SERVER_IS_PREMIUM, "")
        );

        return server;
    }
}
