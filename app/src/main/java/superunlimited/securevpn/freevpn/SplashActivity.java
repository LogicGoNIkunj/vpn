package superunlimited.securevpn.freevpn;

import static superunlimited.securevpn.freevpn.Utils.currency;
import static superunlimited.securevpn.freevpn.Utils.halfyear;
import static superunlimited.securevpn.freevpn.Utils.halfyear_price;
import static superunlimited.securevpn.freevpn.Utils.halfyearkey;
import static superunlimited.securevpn.freevpn.Utils.isPremium;
import static superunlimited.securevpn.freevpn.Utils.monthly;
import static superunlimited.securevpn.freevpn.Utils.monthly_price;
import static superunlimited.securevpn.freevpn.Utils.monthlykey;
import static superunlimited.securevpn.freevpn.Utils.yearly;
import static superunlimited.securevpn.freevpn.Utils.yearly_price;
import static superunlimited.securevpn.freevpn.Utils.yearlykey;
import static superunlimited.securevpn.freevpn.ads.InterstitialManager.sss;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDelegate;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.PurchaseInfo;
import com.google.firebase.FirebaseApp;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.onesignal.OneSignal;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.blinkt.openvpn.core.OpenVPNService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import superunlimited.securevpn.freevpn.ads.InterstitialManager;
import superunlimited.securevpn.freevpn.ads.api.APIInterFace;
import superunlimited.securevpn.freevpn.ads.api.AdsModel;
import superunlimited.securevpn.freevpn.model.Country;

public class SplashActivity extends Activity implements BillingProcessor.IBillingHandler {
    Boolean isActivityLeft;

    SharedPreferences sharedPreference;
    SharedPreferences.Editor editor;
    private BillingProcessor bp;
    private PurchaseInfo purchaseTransactionDetails = null;
    String key = monthlykey;
    int i = 0;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MyApplication.appOpenManager.isShowingAd = true;
        FirebaseApp.initializeApp(this);
        OneSignal.initWithContext(this);
        OneSignal.setAppId(getResources().getString(R.string.ONESIGNAL_APP_ID));
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        FirebaseCrashlytics.getInstance();
        getVPNlist();
        isActivityLeft = false;
        sharedPreference = getSharedPreferences("splash", Context.MODE_PRIVATE);
        editor = sharedPreference.edit();
        bp = new BillingProcessor(this, getResources().getString(R.string.play_console_license), this);
        bp.initialize();
        getAdsData();
        MyApplication.appOpenManager.isShowingAd = true;

        Date date = Calendar.getInstance().getTime();
        @SuppressLint({"NewApi", "LocalSuppress"}) DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        SharedPreferences sp = getSharedPreferences("premium", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();


        if (!sp.getString("date", dateFormat.format(date)).equals(dateFormat.format(date))) {
            editor = sp.edit();
            editor.putString("date", dateFormat.format(date));
            editor.putFloat("data", 0.0f);
            editor.putFloat("ccc", 0.0f);
            editor.putBoolean("used", false);
            editor.apply();
        }
        editor = sp.edit();
        editor.putString("date", dateFormat.format(date));
        editor.apply();


        if (Utils.getispremium(SplashActivity.this)) {
            new Handler().postDelayed(() -> {
                startActivity(new Intent(SplashActivity.this, StartActivity.class));
                finish();
            }, 1000);
        } else {

            if (OpenVPNService.getStatus().equals("CONNECTED")) {
                new Handler().postDelayed(() -> {
                    if (sss) {
                        if (!CheckInternetConnection.netCheck(SplashActivity.this)) {
                            startActivity(new Intent(SplashActivity.this, StartActivity.class));
                        } else {
                            startActivity(new Intent(SplashActivity.this, PremiumActivity.class));
                        }
                        finish();

                        return;
                    }
                    startActivity(new Intent(SplashActivity.this, StartActivity.class));
                    if (!Utils.getispremium(SplashActivity.this)) {
                        InterstitialManager.ShowAdsaa(this, isActivityLeft);
                    }
                    MyApplication.appOpenManager.isShowingAd = false;
                    finish();
                }, 2500);
            } else {
                new Handler().postDelayed(() -> {
                    if (sss) {
                        if (!CheckInternetConnection.netCheck(SplashActivity.this)) {
                            startActivity(new Intent(SplashActivity.this, StartActivity.class));
                        } else {
                            startActivity(new Intent(SplashActivity.this, PremiumActivity.class));
                        }
                        finish();
                        return;
                    }
                    startActivity(new Intent(SplashActivity.this, StartActivity.class));
                    if (!Utils.getispremium(SplashActivity.this)) {
                        InterstitialManager.ShowAdsaa(this, isActivityLeft);
                    }
                    MyApplication.appOpenManager.isShowingAd = false;
                    finish();
                }, 5000);

            }
        }


    }

    @Override
    public void onBackPressed() {

    }

    public void getAdsData() {
        Retrofit.Builder retrofit = new Retrofit.Builder().baseUrl(APIInterFace.adsAPI).addConverterFactory(GsonConverterFactory.create());
        APIInterFace service = retrofit.build().create(APIInterFace.class);
        Call<AdsModel> call = service.getstorylist(5);
        call.enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(Call<AdsModel> call, Response<AdsModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            if (response.body().getData() != null) {
                                if (response.body().getData().getPublishers() != null) {

                                    if (response.body().getData().getPublishers().getAdmob() != null) {
                                        if (response.body().getData().getPublishers().getAdmob().getAdmob_banner() != null) {
                                            MyApplication.set_Admob_banner_Id(response.body().getData().getPublishers().getAdmob().getAdmob_banner().getAdmob_banner_id());
                                        } else {
                                            MyApplication.set_Admob_banner_Id(getString(R.string.banner));
                                        }
                                        if (response.body().getData().getPublishers().getAdmob().getAdmob_interstitial() != null) {
                                            MyApplication.set_Admob_interstitial_Id(response.body().getData().getPublishers().getAdmob().getAdmob_interstitial().getAdmob_interstitial_id());
                                        } else {
                                            MyApplication.set_Admob_interstitial_Id(getString(R.string.splash_interestial_add));
                                        }
                                        if (response.body().getData().getPublishers().getAdmob().getAdmob_native() != null) {
                                            MyApplication.set_Admob_native_Id(response.body().getData().getPublishers().getAdmob().getAdmob_native().getAdmob_Native_id());
                                        } else {
                                            MyApplication.set_Admob_native_Id(getString(R.string.nativead_id));
                                        }
                                        if (response.body().getData().getPublishers().getAdmob().getAdmob_smallnative() != null) {
                                            MyApplication.set_Admob_smallnative_Id(response.body().getData().getPublishers().getAdmob().getAdmob_smallnative().getAdmob_smallNative_id());
                                        } else {
                                            MyApplication.set_Admob_smallnative_Id(getString(R.string.nativead_id));
                                        }
                                        if (response.body().getData().getPublishers().getAdmob().getAdmob_open() != null) {
                                            MyApplication.set_Admob_open_Id(response.body().getData().getPublishers().getAdmob().getAdmob_open().getAdmob_OpenAd_id());
                                        } else {
                                            MyApplication.set_Admob_open_Id("");
                                        }
                                    } else {
                                        MyApplication.set_Admob_banner_Id(getString(R.string.banner));
                                        MyApplication.set_Admob_interstitial_Id(getString(R.string.splash_interestial_add));
                                        MyApplication.set_Admob_native_Id(getString(R.string.nativead_id));
                                        MyApplication.set_Admob_smallnative_Id(getString(R.string.nativead_id));
                                    }
                                } else {
                                    MyApplication.set_Admob_banner_Id(getString(R.string.banner));
                                    MyApplication.set_Admob_interstitial_Id(getString(R.string.splash_interestial_add));
                                    MyApplication.set_Admob_native_Id(getString(R.string.nativead_id));
                                    MyApplication.set_Admob_smallnative_Id(getString(R.string.nativead_id));

                                }

                            } else {
                                MyApplication.set_Admob_banner_Id(getString(R.string.banner));
                                MyApplication.set_Admob_interstitial_Id(getString(R.string.splash_interestial_add));
                                MyApplication.set_Admob_native_Id(getString(R.string.nativead_id));
                                MyApplication.set_Admob_smallnative_Id(getString(R.string.nativead_id));

                            }
                        } else {
                            MyApplication.set_Admob_banner_Id(getString(R.string.banner));
                            MyApplication.set_Admob_interstitial_Id(getString(R.string.splash_interestial_add));
                            MyApplication.set_Admob_native_Id(getString(R.string.nativead_id));
                            MyApplication.set_Admob_smallnative_Id(getString(R.string.nativead_id));

                        }
                    } else {
                        MyApplication.set_Admob_banner_Id(getString(R.string.banner));
                        MyApplication.set_Admob_interstitial_Id(getString(R.string.splash_interestial_add));
                        MyApplication.set_Admob_native_Id(getString(R.string.nativead_id));
                        MyApplication.set_Admob_smallnative_Id(getString(R.string.nativead_id));

                    }
                } else {
                    MyApplication.set_Admob_banner_Id(getString(R.string.banner));
                    MyApplication.set_Admob_interstitial_Id(getString(R.string.splash_interestial_add));
                    MyApplication.set_Admob_native_Id(getString(R.string.nativead_id));
                    MyApplication.set_Admob_smallnative_Id(getString(R.string.nativead_id));

                }

            }

            @Override
            public void onFailure(Call<AdsModel> call, Throwable t) {
                MyApplication.set_Admob_banner_Id(getString(R.string.banner));
                MyApplication.set_Admob_interstitial_Id(getString(R.string.splash_interestial_add));
                MyApplication.set_Admob_native_Id(getString(R.string.nativead_id));
                MyApplication.set_Admob_smallnative_Id(getString(R.string.nativead_id));

            }
        });
    }


    protected void onPause() {
        super.onPause();
        isActivityLeft = true;

    }

    protected void onStop() {
        super.onStop();
        isActivityLeft = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityLeft = false;
    }

    protected void onDestroy() {
        super.onDestroy();
        if (bp != null) {
            bp.release();
        }
        isActivityLeft = true;
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable PurchaseInfo details) {

    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

        try {
            purchaseTransactionDetails = bp.getSubscriptionPurchaseInfo(key);
            Calendar calendar = Calendar.getInstance();

            if (i == 0) {
                key = monthlykey;
                try {
                    calendar.setTime(purchaseTransactionDetails.purchaseData.purchaseTime);
                    calendar.add(Calendar.MONTH, 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (i == 1) {
                key = monthlykey;
                try {
                    calendar.setTime(purchaseTransactionDetails.purchaseData.purchaseTime);
                    calendar.add(Calendar.MONTH, 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (i == 2) {
                key = halfyearkey;
                try {
                    calendar.setTime(purchaseTransactionDetails.purchaseData.purchaseTime);
                    calendar.add(Calendar.MONTH, 6);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (i == 3) {
                key = yearlykey;
                try {
                    calendar.setTime(purchaseTransactionDetails.purchaseData.purchaseTime);
                    calendar.add(Calendar.MONTH, 12);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            i++;
            purchaseTransactionDetails = bp.getSubscriptionPurchaseInfo(key);
            bp.loadOwnedPurchasesFromGoogleAsync(new BillingProcessor.IPurchasesResponseListener() {
                @Override
                public void onPurchasesSuccess() {
                }

                @Override
                public void onPurchasesError() {
                }
            });

            if (hasSubscription()) {
                i = 4;
                isPremium = true;
                Utils.setispremium(SplashActivity.this, isPremium);

                String formateDate = new SimpleDateFormat("dd-MMM-yyyy").format(calendar.getTime());

                Utils.subscribe_time = formateDate;


            } else {
                isPremium = false;
                Utils.setispremium(SplashActivity.this, isPremium);
            }

            if (i != 4) {
                bp = new BillingProcessor(this, getResources().getString(R.string.play_console_license), this);
                bp.initialize();
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

    }

    private boolean hasSubscription() {
        if (purchaseTransactionDetails != null) {
            return purchaseTransactionDetails.purchaseData != null;
        }
        return false;
    }

    void getVPNlist() {

        VideoStatusServiceP videoStatusServiceP = VideoStatusClient.getClient().create(VideoStatusServiceP.class);
        videoStatusServiceP.getCountryList(MyApplication.MYSECRET).enqueue(new Callback<Country>() {
            @Override
            public void onResponse(@NotNull Call<Country> call, @NotNull Response<Country> response) {

                try {
                    if (response.code() == 200) {

                        if (response.isSuccessful()) {

                            monthly = response.body().getPrice().getMonthly();
                            halfyear = response.body().getPrice().getHalfyear();
                            yearly = response.body().getPrice().getYearly();

                            monthlykey = response.body().getPrice().getMonthlykey();
                            halfyearkey = response.body().getPrice().getHalfyearkey();
                            yearlykey = response.body().getPrice().getYearlykey();


                            monthly_price = response.body().getPrice().getMonthly_price();
                            halfyear_price = response.body().getPrice().getHalfyear_price();
                            yearly_price = response.body().getPrice().getYearly_price();

                            currency = response.body().getPrice().getCurrency();

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<Country> call, @NotNull Throwable t) {

            }
        });
    }

}
