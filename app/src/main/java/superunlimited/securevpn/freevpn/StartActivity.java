package superunlimited.securevpn.freevpn;

import static android.view.View.VISIBLE;
import static superunlimited.securevpn.freevpn.ads.InterstitialManager.admobinterstitialAd;
import static superunlimited.securevpn.freevpn.ads.InterstitialManager.sss;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

import de.blinkt.openvpn.core.OpenVPNService;
import retrofit2.Call;
import retrofit2.Callback;
import superunlimited.securevpn.freevpn.ads.InterstitialManager;
import superunlimited.securevpn.freevpn.interfaces.ChangeCountry;
import superunlimited.securevpn.freevpn.interfaces.ChangeServer;
import superunlimited.securevpn.freevpn.interfaces.Openlist;
import superunlimited.securevpn.freevpn.interfaces.StartServer;
import superunlimited.securevpn.freevpn.model.BtnModel;
import superunlimited.securevpn.freevpn.model.Server;

public class StartActivity extends AppCompatActivity implements ChangeServer, Openlist {
    public static boolean check = false;
    public static superunlimited.securevpn.freevpn.NonSwipeableViewPager pager;
    StartServer startServer;
    ChangeCountry changeCountry;
    CheckInternetConnection CheckInternetConnection;
    ImageView drawermanu,close;
    RelativeLayout playgames;
    RelativeLayout drawerlayout;
    int prem = 0;
    public static int count = 0;

    public void setSendData(StartServer startServer) {
        this.startServer = startServer;
    }

    public void setSendDatas(ChangeCountry changeCountry) {
        this.changeCountry = changeCountry;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        getWindow().setBackgroundDrawable(null);

        if (!Utils.getispremium(StartActivity.this)) {
            try {
                if (admobinterstitialAd != null) {
                    if (sss) {
                        InterstitialManager.LoadAdsaaa(StartActivity.this);
                        sss = false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Intent intent = new Intent(this, OpenVPNService.class);
        intent.setAction(OpenVPNService.START_SERVICE);
        getApplicationContext().startService(intent);
        prem = getIntent().getIntExtra("premium", 0);

        pager = (superunlimited.securevpn.freevpn.NonSwipeableViewPager) findViewById(R.id.viewPager);
        drawermanu = findViewById(R.id.drawer);
        close = findViewById(R.id.close);
        drawerlayout = findViewById(R.id.drawerlayout);
        CheckInternetConnection = new CheckInternetConnection();
        CheckInternetConnet();
        PlayGames();

        drawermanu.setOnClickListener(v -> {
            drawerlayout.setVisibility(VISIBLE);
        });
        close.setOnClickListener(v -> {
            drawerlayout.setVisibility(View.GONE);
        });
        findViewById(R.id.vpn).setOnClickListener(v -> pager.setCurrentItem(1));
        findViewById(R.id.home).setOnClickListener(v -> pager.setCurrentItem(0));
        findViewById(R.id.pro).setOnClickListener(v -> pager.setCurrentItem(2));
        findViewById(R.id.myaccount).setOnClickListener(v -> startActivity(new Intent(StartActivity.this,MyAccoutActivity.class)));

        if (Utils.getispremium(StartActivity.this)) {
            findViewById(R.id.subscription).setVisibility(View.GONE);
            findViewById(R.id.playgame).setVisibility(View.GONE);
        }
        playgames = findViewById(R.id.playgame);

        findViewById(R.id.privacy).setOnClickListener(v -> {
            String url = "https://sites.google.com/view/learningtosmart/home";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });
        findViewById(R.id.feedback).setOnClickListener(v -> {
            startActivity(new Intent(StartActivity.this, ContactUsActivity.class));
        });
        findViewById(R.id.subscription).setOnClickListener(v -> {
            drawerlayout.setVisibility(View.GONE);
            drawermanu.setVisibility(View.GONE);
            pager.setCurrentItem(2);
        });
        findViewById(R.id.rateapp).setOnClickListener(v -> {
            try {
                new RateDialog(StartActivity.this, false).show();
            } catch (Exception anfe) {
                anfe.printStackTrace();
            }
        });
        findViewById(R.id.shareapp).setOnClickListener(v -> {
            Intent intent1 = new Intent(Intent.ACTION_SEND);
            intent1.setType("text/plain");
            String app_urls = " https://play.google.com/store/apps/details?id=" + getPackageName();
            intent1.putExtra(Intent.EXTRA_TEXT, app_urls);
            startActivity(Intent.createChooser(intent1, "Share via"));
        });

    }

    private void PlayGames() {
        VideoStatusServiceP videoStatusService = VideoStatusClient.getClientQuereka().create(VideoStatusServiceP.class);
        videoStatusService.getBtnAd().enqueue(new Callback<BtnModel>() {
            @Override
            public void onResponse(@NotNull Call<BtnModel> call, retrofit2.@NotNull Response<BtnModel> response) {
                try {
                    assert response.body() != null;
                    if (response.body().isStatus()) {

                        if (response.body().getData().isFlage()) {
                            if (!Utils.getispremium(StartActivity.this)) {
                                playgames.setVisibility(View.VISIBLE);
                            }
                            try {
                                if (!isFinishing()) {
                                    Glide.with(StartActivity.this).load(response.body().getData().getImage()).into((ImageView) findViewById(R.id.btnopenad));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            playgames.setOnClickListener(v -> {
                                String url = response.body().getData().getUrl();
                                try {
                                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                                    CustomTabsIntent customTabsIntent = builder.build();
                                    customTabsIntent.intent.setPackage("com.android.chrome");
                                    customTabsIntent.launchUrl(StartActivity.this, Uri.parse(url));
                                } catch (Exception e) {
                                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                                    CustomTabsIntent customTabsIntent = builder.build();
                                    customTabsIntent.launchUrl(StartActivity.this, Uri.parse(url));
                                }
                            });
                        } else {
                            playgames.setVisibility(View.GONE);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<BtnModel> call, @NotNull Throwable t) {
            }
        });
    }

    private void CheckInternetConnet() {

        if (!CheckInternetConnection.netCheck(StartActivity.this)) {
            Dialog nointernetDialog = new Dialog(StartActivity.this, R.style.AppTheme);
            nointernetDialog.requestWindowFeature(1);
            nointernetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            nointernetDialog.setContentView(R.layout.dialog_nointernet);
            nointernetDialog.setCancelable(false);
            nointernetDialog.setCanceledOnTouchOutside(false);
            LinearLayout dialog_ll = nointernetDialog.findViewById(R.id.dialog_ll);
            dialog_ll.setOnClickListener(v -> {
                nointernetDialog.dismiss();
                CheckInternetConnet();
            });
            nointernetDialog.show();
        } else {
            pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(), 1));
            if (prem == 1) {
                drawermanu.setVisibility(View.GONE);
                pager.setCurrentItem(2);
            } else {
                pager.setCurrentItem(0);
            }
            pager.setOffscreenPageLimit(5);
            pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {


                    if (position == 0) {
                        if (!isFinishing()) {
                            if (!Utils.getispremium(StartActivity.this)) {
                                playgames.setVisibility(View.VISIBLE);
                            }
                            drawermanu.setVisibility(VISIBLE);
                        }
                    } else if (position == 2) {
                        playgames.setVisibility(View.GONE);
                        drawermanu.setVisibility(View.GONE);
                    } else {
                        if (!isFinishing()) {
                            drawermanu.setVisibility(View.GONE);
                        }
                    }

                    if (!Utils.getispremium(StartActivity.this)) {
                        
                        if (count > 8) {
                            count = 0;
                            drawermanu.setVisibility(View.GONE);
                            pager.setCurrentItem(2);
                        }
                    }

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }

    }


    @Override
    public void newServer(Server server) {

        if (server == null) {
            if (!Utils.getispremium(StartActivity.this)) {
                drawermanu.setVisibility(View.GONE);
                pager.setCurrentItem(2);
            }
        } else {
            pager.setCurrentItem(0);
            startServer.StartnewServer(server);
            changeCountry.newCountry("s");
        }
    }

    @Override
    public void openvpnlist(String s) {
        pager.setCurrentItem(1);
    }

    @Override
    public void onBackPressed() {

        if (pager.getCurrentItem() == 2) {
            pager.setCurrentItem(0);
            return;
        }
        if (drawerlayout.getVisibility()==VISIBLE) {
            drawerlayout.setVisibility(View.GONE);
        } else {
            if (pager.getCurrentItem() == 1) {
                pager.setCurrentItem(0);
            } else {
                finish();
            }

        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    return HomeFragment.newInstance("home");
                case 1:
                    return VPNFragment.newInstance("vpn", "sss");
                case 2:
                    return PremiumFragment.newInstance("premium");
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 3;

        }
    }

}