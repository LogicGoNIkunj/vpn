package superunlimited.securevpn.freevpn;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import java.io.File;

public class Utils {

    public static boolean isPremium = false;
    public static int currenttime;
    public static String monthly = "00";
    public static String halfyear = "00";
    public static String yearly = "00";
    public static String currency = "INR";
    public static String monthly_price = "00";
    public static String halfyear_price = "00";
    public static String yearly_price = "00";
    public static String monthlykey = "vpn_montly_primumum";
    public static String halfyearkey = "vpn_sixmonth_primumum";
    public static String yearlykey = "vpn_yearly_primumum";
    public static String subscribe_time = "XX-XXX-XXXX";

    public static String getDataDirectoryPath(Activity activity) {
        String absolutePath = activity.getCacheDir().getAbsolutePath();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(absolutePath);
        stringBuilder.append(File.separator);
        stringBuilder.append(activity.getResources().getString(R.string.root_directory));
        File file = new File(stringBuilder.toString());
        if (!file.exists()) {
            file.mkdirs();
        }
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(file.getAbsolutePath());
        stringBuilder2.append(File.separator);
        return stringBuilder2.toString();
    }


    public static boolean getispremium(Context context){
        SharedPreferences sharedPreference = context.getSharedPreferences("premium",Context.MODE_PRIVATE);
        return sharedPreference.getBoolean("pm",false);
    }

    public static void setispremium(Context context,boolean premiummm){
        SharedPreferences sharedPreference = context.getSharedPreferences("premium",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putBoolean("pm",premiummm);
        editor.apply();
    }

}
