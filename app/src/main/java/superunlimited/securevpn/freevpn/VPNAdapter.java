package superunlimited.securevpn.freevpn;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import superunlimited.securevpn.freevpn.interfaces.ChangeServer;
import superunlimited.securevpn.freevpn.model.Country;
import superunlimited.securevpn.freevpn.model.Server;

public class VPNAdapter extends RecyclerView.Adapter<VPNAdapter.MyViewHolder> {
    Activity context;
    String filename;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private List<Country.Datum> datumList = new ArrayList();
    private ChangeServer changeServer;
    private SharedPreference preference;
    ProgressDialog pd;

    public VPNAdapter(Context context, ArrayList<Country.Datum> categoryModellist) {
        this.context = (Activity) context;
        this.datumList = categoryModellist;
        changeServer = (ChangeServer) context;
        preference = new SharedPreference(this.context);
        sharedPreferences = context.getSharedPreferences("FirstTime", Context.MODE_PRIVATE);
        pd = new ProgressDialog(context);
    }

    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rowView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vpnlist, viewGroup, false);
        return new MyViewHolder(rowView);
    }

    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, @SuppressLint("RecyclerView") int i) {

        Glide.with(this.context).load(datumList.get(i).getFlag()).into(myViewHolder.countryimage);

        myViewHolder.countryname.setText(datumList.get(i).getCountry());
        myViewHolder.time.setText(datumList.get(i).getTime());
        myViewHolder.text.setText(datumList.get(i).getText());

        if (Utils.getispremium(context)) {
            myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {


                    List<File> files = Arrays.asList(new File(Utils.getDataDirectoryPath(context)).listFiles());

                    boolean checked = false;
                    String path = "";

                    for (int j = 0; j < files.size(); j++) {

                        if (datumList.get(i).getFile().contains(files.get(j).getPath().substring(files.get(j).getPath().lastIndexOf('/') + 1))) {
                            path = files.get(j).getPath();
                            checked = true;

                        }
                    }

                    if (checked) {
                        preference.saveServer(new Server(datumList.get(i).getCountry(),
                                datumList.get(i).getID(),
                                datumList.get(i).getFlag(),
                                path,
                                "vpn",
                                "vpn",
                                datumList.get(i).getIp(),
                                datumList.get(i).getTime(),
                                datumList.get(i).getText(),
                                datumList.get(i).getIs_premium()
                        ));

                        changeServer.newServer(new Server(datumList.get(i).getCountry(),
                                datumList.get(i).getID(),
                                datumList.get(i).getFlag(),
                                path,
                                "vpn",
                                "vpn",
                                datumList.get(i).getIp(),
                                datumList.get(i).getTime(),
                                datumList.get(i).getText(),
                                datumList.get(i).getIs_premium()

                        ));


                    } else {
                        filename = datumList.get(i).getFile().substring(datumList.get(i).getFile().lastIndexOf('/') + 1);
                        new DownloadFileFromURL(datumList.get(i).getFile(), i).execute();
                    }

                    notifyDataSetChanged();

                }

            });
        } else {
            if (datumList.get(i).getIs_premium().equals("1")) {
                myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        changeServer.newServer(null);
                    }
                });

            } else
                {
                myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        List<File> files = Arrays.asList(new File(Utils.getDataDirectoryPath(context)).listFiles());

                        boolean checked = false;
                        String path = "";

                        for (int j = 0; j < files.size(); j++) {

                            if (datumList.get(i).getFile().contains(files.get(j).getPath().substring(files.get(j).getPath().lastIndexOf('/') + 1))) {
                                path = files.get(j).getPath();
                                checked = true;

                            }
                        }

                        if (checked) {
                            preference.saveServer(new Server(datumList.get(i).getCountry(),
                                    datumList.get(i).getID(),
                                    datumList.get(i).getFlag(),
                                    path,
                                    "vpn",
                                    "vpn",
                                    datumList.get(i).getIp(),
                                    datumList.get(i).getTime(),
                                    datumList.get(i).getText(),
                                    datumList.get(i).getIs_premium()
                            ));

                            changeServer.newServer(new Server(datumList.get(i).getCountry(),
                                    datumList.get(i).getID(),
                                    datumList.get(i).getFlag(),
                                    path,
                                    "vpn",
                                    "vpn",
                                    datumList.get(i).getIp(),
                                    datumList.get(i).getTime(),
                                    datumList.get(i).getText(),
                                    datumList.get(i).getIs_premium()

                            ));


                        } else {
                            filename = datumList.get(i).getFile().substring(datumList.get(i).getFile().lastIndexOf('/') + 1);
                            new DownloadFileFromURL(datumList.get(i).getFile(), i).execute();
                        }

                        notifyDataSetChanged();

                    }

                });
            }

        }


        if (datumList.get(i).getIs_premium().equals("1")) {
            Glide.with(this.context).load(context.getResources().getDrawable(R.drawable.ic_rank)).into(myViewHolder.is_premium);
        }else {
            Glide.with(this.context).load(context.getResources().getDrawable(R.drawable.ic_rankddddd)).into(myViewHolder.is_premium);
        }

            if (preference.getServer().getID().equals(datumList.get(i).getID())) {
            Glide.with(this.context).load(context.getResources().getDrawable(R.drawable.select_btn_01)).into(myViewHolder.checked);
            myViewHolder.countryname.setTypeface(Typeface.DEFAULT_BOLD);
        } else {
            Glide.with(this.context).load(context.getResources().getDrawable(R.drawable.ic_uncheck_01)).into(myViewHolder.checked);
            myViewHolder.countryname.setTypeface(Typeface.DEFAULT);
        }


        if (!sharedPreferences.getBoolean("recordPref", false)) {
            List<File> files = Arrays.asList(new File(Utils.getDataDirectoryPath(context)).listFiles());
            boolean checked = false;
            for (int j = 0; j < files.size(); j++) {

                if (datumList.get(i).getFile().contains(files.get(j).getPath().substring(files.get(j).getPath().lastIndexOf('/') + 1))) {
                    checked = true;

                }
            }
            if (!checked) {
                filename = datumList.get(0).getFile().substring(datumList.get(0).getFile().lastIndexOf('/') + 1);
                new DownloadFileFromURL(datumList.get(0).getFile(), 0).execute();
            }

            editor = sharedPreferences.edit();
            editor.putBoolean("recordPref", true);
            editor.apply();

        }

    }

    public int getItemCount() {
        return this.datumList.size();
    }

    public void filterList(List<Country.Datum> filterllist) {
        // below line is to add our filtered
        // list in our course array list.
        datumList = filterllist;
        // below line is to notify our adapter
        // as change in recycler view data.
        notifyDataSetChanged();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView countryimage, checked, is_premium;
        TextView countryname, time, text;

        MyViewHolder(View view) {
            super(view);
            countryimage = view.findViewById(R.id.countryflag);
            countryname = view.findViewById(R.id.countryname);
            checked = view.findViewById(R.id.checked);
            time = view.findViewById(R.id.time);
            text = view.findViewById(R.id.text);
            is_premium = view.findViewById(R.id.is_premium);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        String videoPath;
        int position = 0;


        DownloadFileFromURL(String videoPath, int position) {
            this.videoPath = videoPath;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("setting up vpn server...");
            pd.setCancelable(true);
            pd.show();
        }

        @Override
        protected String doInBackground(String... f_url) {

            if (CheckInternetConnection.netCheck(context)) {

                PRDownloader.download(videoPath, Utils.getDataDirectoryPath(context), filename)
                        .build()
                        .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                            @Override
                            public void onStartOrResume() {

                            }
                        })
                        .setOnPauseListener(new OnPauseListener() {
                            @Override
                            public void onPause() {

                            }
                        })
                        .setOnCancelListener(new OnCancelListener() {
                            @Override
                            public void onCancel() {

                            }
                        })
                        .setOnProgressListener(new OnProgressListener() {
                            @Override
                            public void onProgress(Progress progress) {

                            }
                        })
                        .start(new OnDownloadListener() {
                            @Override
                            public void onDownloadComplete() {

                                try {
                                    pd.dismiss();
                                    preference.saveServer(new Server(datumList.get(position).getCountry(),
                                            datumList.get(position).getID(),
                                            datumList.get(position).getFlag(),
                                            Utils.getDataDirectoryPath(context) + filename,
                                            "vpn",
                                            "vpn",
                                            datumList.get(position).getIp(),
                                            datumList.get(position).getTime(),
                                            datumList.get(position).getText(),
                                            datumList.get(position).getIs_premium()

                                    ));

                                    changeServer.newServer(new Server(datumList.get(position).getCountry(),
                                            datumList.get(position).getID(),
                                            datumList.get(position).getFlag(),
                                            Utils.getDataDirectoryPath(context) + filename,
                                            "vpn",
                                            "vpn",
                                            datumList.get(position).getIp(),
                                            datumList.get(position).getTime(),
                                            datumList.get(position).getText(),
                                            datumList.get(position).getIs_premium()
                                    ));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onError(Error error) {
                            }

                        });


            } else {
                pd.dismiss();
            }


            return null;
        }


        @Override
        protected void onPostExecute(String file_url) {
        }
    }


}

