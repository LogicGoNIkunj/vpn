package superunlimited.securevpn.freevpn;

import static superunlimited.securevpn.freevpn.StartActivity.count;
import static superunlimited.securevpn.freevpn.Utils.currency;
import static superunlimited.securevpn.freevpn.Utils.halfyear;
import static superunlimited.securevpn.freevpn.Utils.halfyear_price;
import static superunlimited.securevpn.freevpn.Utils.halfyearkey;
import static superunlimited.securevpn.freevpn.Utils.monthly;
import static superunlimited.securevpn.freevpn.Utils.monthly_price;
import static superunlimited.securevpn.freevpn.Utils.monthlykey;
import static superunlimited.securevpn.freevpn.Utils.yearly;
import static superunlimited.securevpn.freevpn.Utils.yearly_price;
import static superunlimited.securevpn.freevpn.Utils.yearlykey;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import superunlimited.securevpn.freevpn.interfaces.ChangeCountry;
import superunlimited.securevpn.freevpn.model.Country;


public class VPNFragment extends Fragment implements ChangeCountry {

    RecyclerView vpnrecyclerview;
    VPNAdapter vpnAdapter;
    ArrayList<Country.Datum> vpnlist = new ArrayList<>();
    SearchView searchView;
    ImageView back;

    public static VPNFragment newInstance(String param1, String param2) {
        VPNFragment fragment = new VPNFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vpn, container, false);

        vpnrecyclerview = view.findViewById(R.id.vpnrecyclerview);

        if (requireActivity() instanceof StartActivity) {
            ((StartActivity) requireActivity()).setSendDatas(this);
        }

        getVPNlist();
        searchView = (SearchView) view.findViewById(R.id.searchView);
        back = (ImageView) view.findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().onBackPressed();

            }
        });


        return view;
    }

    void getVPNlist() {
        VideoStatusServiceP videoStatusServiceP = VideoStatusClient.getClient().create(VideoStatusServiceP.class);
        videoStatusServiceP.getCountryList(MyApplication.MYSECRET).enqueue(new Callback<Country>() {
            @Override
            public void onResponse(@NotNull Call<Country> call, @NotNull Response<Country> response) {

                try {
                    if (response.code() == 200) {

                        if (response.isSuccessful()) {

                            vpnlist = (ArrayList<Country.Datum>) response.body().getData();

                            monthly = response.body().getPrice().getMonthly();
                            halfyear = response.body().getPrice().getHalfyear();
                            yearly = response.body().getPrice().getYearly();

                            monthlykey = response.body().getPrice().getMonthlykey();
                            halfyearkey = response.body().getPrice().getHalfyearkey();
                            yearlykey = response.body().getPrice().getYearlykey();


                            monthly_price = response.body().getPrice().getMonthly_price();
                            halfyear_price = response.body().getPrice().getHalfyear_price();
                            yearly_price = response.body().getPrice().getYearly_price();

                            currency = response.body().getPrice().getCurrency();

                            if (vpnlist != null) {
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                                vpnrecyclerview.setLayoutManager(linearLayoutManager);
                                vpnAdapter = new VPNAdapter(getContext(), vpnlist);
                                vpnrecyclerview.setAdapter(vpnAdapter);
                                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                                    @Override
                                    public boolean onQueryTextSubmit(String query) {

                                        return false;
                                    }

                                    @Override
                                    public boolean onQueryTextChange(String newText) {
                                        filter(newText);
                                        return false;
                                    }
                                });
                            } else {
                                Toast.makeText(getContext(), "No Data Found", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<Country> call, @NotNull Throwable t) {

            }
        });
    }

    @Override
    public void newCountry(String s) {
        vpnAdapter.notifyDataSetChanged();
    }

    private void filter(String text) {
        ArrayList<Country.Datum> filteredlist = new ArrayList<>();

        for (Country.Datum item : vpnlist) {
            if (item.getCountry().toLowerCase().contains(text.toLowerCase())) {
                filteredlist.add(item);
            }
        }
        if (filteredlist.isEmpty()) {

            Toast.makeText(requireActivity(), "No Data Found..", Toast.LENGTH_SHORT).show();
        } else {

            vpnAdapter.filterList(filteredlist);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        count++;
        if (HomeFragment.refreshadapter) {
            if (vpnAdapter != null) {
                vpnAdapter.notifyDataSetChanged();
            }
            HomeFragment.refreshadapter = false;
        }

    }
}
  


