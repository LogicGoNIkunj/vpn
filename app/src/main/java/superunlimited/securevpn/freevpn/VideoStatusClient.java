package superunlimited.securevpn.freevpn;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VideoStatusClient {
    public static String BASE_URL = "http://punchapp.in/api/";
    private static Retrofit retrofit;
   public static String BASE_URLQUEREKA = "https://smartadz.in/api/";
    private static Retrofit retrofit2;

    public static Retrofit getClient() {
        if (retrofit == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }



    public static Retrofit getClientQuereka() {
        if (retrofit2 == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            retrofit2 = new Retrofit.Builder()
                    .baseUrl(BASE_URLQUEREKA)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit2;
    }



}