package superunlimited.securevpn.freevpn;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import superunlimited.securevpn.freevpn.model.BtnModel;
import superunlimited.securevpn.freevpn.model.Country;
import superunlimited.securevpn.freevpn.model.Feedback;

public interface VideoStatusServiceP {

    @GET("vpn-list")
    Call<Country> getCountryList(@Header("AuthorizationKey") String key);


    @FormUrlEncoded
    @POST("feedback")
    Call<Feedback> sendfeedback(
            @Field("app_name") String app_name,
            @Field("package_name") String package_name,
            @Field("title") String title,
            @Field("description") String description,
            @Field("device_name") String device_name,
            @Field("android_version") String android_version,
            @Field("version") String version
    );

    @GET("qureka-ad")
    Call<BtnModel> getBtnAd();

}
