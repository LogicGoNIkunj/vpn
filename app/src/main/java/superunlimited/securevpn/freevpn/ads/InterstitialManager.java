package superunlimited.securevpn.freevpn.ads;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import superunlimited.securevpn.freevpn.MyApplication;
import superunlimited.securevpn.freevpn.Utils;

public class InterstitialManager {
    public static InterstitialAd admobinterstitialAd;
    public static boolean sss = false;


    public static void LoadAdsaaa(Context activity) {
        if (!Utils.getispremium(activity)) {

            if (!MyApplication.get_Admob_interstitial_Id().equals("")) {
                admob_Interstitial(activity);
            }
        }
    }

    public static void ShowAdsaa(Context activity, boolean isActivityLeft) {
        try {
            if (admobinterstitialAd != null && !isActivityLeft) {
                admobinterstitialAd.show((Activity) activity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void admob_Interstitial(Context activity) {

        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(activity, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                admobinterstitialAd = interstitialAd;
                admobinterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        MyApplication.appOpenManager.isShowingAd = false;
                        admobinterstitialAd = null;
                        if (!Utils.getispremium(activity)) {
                            InterstitialManager.LoadAdsaaa(activity);
                        }
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        MyApplication.appOpenManager.isShowingAd = true;
                    }

                    @Override
                    public void onAdImpression() {

                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {

                        sss = true;


                    }
                });

            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                admobinterstitialAd = null;
                sss = true;
            }
        });
    }
}