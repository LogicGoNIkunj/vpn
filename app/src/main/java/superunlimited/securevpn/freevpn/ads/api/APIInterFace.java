package superunlimited.securevpn.freevpn.ads.api;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIInterFace {
    String adsAPI = "https://stage-ads.punchapp.in/api/";
    @FormUrlEncoded
    @POST("get-ads-list")
    Call<AdsModel> getstorylist(@Field("app_id") int IntVal);
}