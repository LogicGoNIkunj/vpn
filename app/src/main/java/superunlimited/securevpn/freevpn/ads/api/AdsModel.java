package superunlimited.securevpn.freevpn.ads.api;

import com.google.gson.annotations.SerializedName;

public class AdsModel {
    boolean status;
    String message;
    Data data;

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data {

        int publisher_id;
        String publisher_name;
        publishers publishers;

        public int getPublisher_id() {
            return publisher_id;
        }

        public String getPublisher_name() {
            return publisher_name;
        }

        public Data.publishers getPublishers() {
            return publishers;
        }

        public class publishers {
            @SerializedName("admob")
            Admob admob;

            public Admob getAdmob() {
                return admob;
            }

            public class Admob {
                @SerializedName("banner")
                Admob_Banner banner;
                @SerializedName("interstitial")
                Admob_Interstitial admob_interstitial;
                @SerializedName("open")
                Admob_OpenAd admob_open;
                @SerializedName("native")
                Admob_Native admob_native;
                @SerializedName("native_small")
                Admob_SmallNative admob_smallnative;

                public Admob_Banner getAdmob_banner() {
                    return banner;
                }

                public Admob_Interstitial getAdmob_interstitial() {
                    return admob_interstitial;
                }

                public Admob_OpenAd getAdmob_open() {
                    return admob_open;
                }

                public Admob_Native getAdmob_native() {
                    return admob_native;
                }

                public Admob_SmallNative getAdmob_smallnative() {
                    return admob_smallnative;
                }

                public class Admob_Banner {
                    @SerializedName("id")
                    String admob_banner_id;
                    @SerializedName("show_time")
                    int admob_banner_show_time;

                    public String getAdmob_banner_id() {
                        return admob_banner_id;
                    }

                    public int getAdmob_banner_show_time() {
                        return admob_banner_show_time;
                    }
                }
                public class Admob_Interstitial {
                    @SerializedName("id")
                    String admob_interstitial_id;
                    @SerializedName("show_time")
                    int admob_interstitial_show_time;

                    public String getAdmob_interstitial_id() {
                        return admob_interstitial_id;
                    }

                    public int getAdmob_interstitial_show_time() {
                        return admob_interstitial_show_time;
                    }
                }
                public class Admob_OpenAd {
                    @SerializedName("id")
                    String admob_OpenAd_id;
                    @SerializedName("show_time")
                    int admob_OpenAd_show_time;

                    public String getAdmob_OpenAd_id() {
                        return admob_OpenAd_id;
                    }

                    public int getAdmob_OpenAd_show_time() {
                        return admob_OpenAd_show_time;
                    }
                }

                public class Admob_Native {
                    @SerializedName("id")
                    String admob_Native_id;
                    @SerializedName("show_time")
                    int admob_Native_show_time;

                    public String getAdmob_Native_id() {
                        return admob_Native_id;
                    }

                    public int getAdmob_Native_show_time() {
                        return admob_Native_show_time;
                    }
                }

                public class Admob_SmallNative {
                    @SerializedName("id")
                    String admob_smallNative_id;
                    @SerializedName("show_time")
                    int admob_smallNative_show_time;

                    public String getAdmob_smallNative_id() {
                        return admob_smallNative_id;
                    }

                    public int getAdmob_smallNative_show_time() {
                        return admob_smallNative_show_time;
                    }
                }
            }
        }
    }
}