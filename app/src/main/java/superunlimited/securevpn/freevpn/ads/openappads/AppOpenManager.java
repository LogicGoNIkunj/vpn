package superunlimited.securevpn.freevpn.ads.openappads;

import static androidx.lifecycle.Lifecycle.Event.ON_START;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import superunlimited.securevpn.freevpn.CheckActivity;
import superunlimited.securevpn.freevpn.MyApplication;
import superunlimited.securevpn.freevpn.Utils;

public class AppOpenManager implements LifecycleObserver, Application.ActivityLifecycleCallbacks {
    public Activity currentActivity;
    public boolean isShowingAd = false;

    public AppOpenManager(MyApplication myApplication) {
        myApplication.registerActivityLifecycleCallbacks(this);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

    }

    @OnLifecycleEvent(ON_START)
    public void onStart() {
        String s = currentActivity + "";
        if (!Utils.getispremium(currentActivity)) {
            showAdIfAvailable();
        }
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle bundle) {

    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        currentActivity = activity;

    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        currentActivity = activity;

    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        currentActivity = null;

    }

    public void showAdIfAvailable() {
        if (!isShowingAd) {
            currentActivity.startActivity(new Intent(currentActivity, CheckActivity.class));
        }
    }

}
