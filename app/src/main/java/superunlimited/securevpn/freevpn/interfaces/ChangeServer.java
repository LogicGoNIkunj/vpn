package superunlimited.securevpn.freevpn.interfaces;

import superunlimited.securevpn.freevpn.model.Server;

public interface ChangeServer {
    void newServer(Server server);
}
