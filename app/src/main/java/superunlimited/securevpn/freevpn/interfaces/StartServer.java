package superunlimited.securevpn.freevpn.interfaces;

import superunlimited.securevpn.freevpn.model.Server;

public interface StartServer {
    void StartnewServer(Server server);
}
