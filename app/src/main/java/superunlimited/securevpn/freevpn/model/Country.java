package superunlimited.securevpn.freevpn.model;

import java.util.List;

public class Country {
    public boolean status;
    public String message;
    public List<Datum> data;
    public Price price;

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {
        public String id;
        public String flag;
        public String country;
        public String ip;
        public String file;
        public String time;
        public String text;
        public String is_premium;

        public String getIs_premium() {
            return is_premium;
        }

        public void setIs_premium(String is_premium) {
            this.is_premium = is_premium;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getID() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }
    }

    public class Price {
        public String monthly;
        public String halfyear;
        public String yearly;
        public String monthlykey;
        public String halfyearkey;
        public String yearlykey;
        public String currency;
        public String monthly_price;
        public String halfyear_price;
        public String yearly_price;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getMonthly_price() {
            return monthly_price;
        }

        public void setMonthly_price(String monthly_price) {
            this.monthly_price = monthly_price;
        }

        public String getHalfyear_price() {
            return halfyear_price;
        }

        public void setHalfyear_price(String halfyear_price) {
            this.halfyear_price = halfyear_price;
        }

        public String getYearly_price() {
            return yearly_price;
        }

        public void setYearly_price(String yearly_price) {
            this.yearly_price = yearly_price;
        }

        public String getMonthlykey() {
            return monthlykey;
        }

        public void setMonthlykey(String monthlykey) {
            this.monthlykey = monthlykey;
        }

        public String getHalfyearkey() {
            return halfyearkey;
        }

        public void setHalfyearkey(String halfyearkey) {
            this.halfyearkey = halfyearkey;
        }

        public String getYearlykey() {
            return yearlykey;
        }

        public void setYearlykey(String yearlykey) {
            this.yearlykey = yearlykey;
        }

        public String getMonthly() {
            return monthly;
        }

        public void setMonthly(String monthly) {
            this.monthly = monthly;
        }

        public String getHalfyear() {
            return halfyear;
        }

        public void setHalfyear(String halfyear) {
            this.halfyear = halfyear;
        }

        public String getYearly() {
            return yearly;
        }

        public void setYearly(String yearly) {
            this.yearly = yearly;
        }
    }


}
