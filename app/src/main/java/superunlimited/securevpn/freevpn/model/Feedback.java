package superunlimited.securevpn.freevpn.model;

import com.google.gson.annotations.SerializedName;

public class Feedback {
    @SerializedName("status")
    private Boolean status;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private datas data;

    public String getMessage() {
        return message;
    }

    public Boolean getStatus() {
        return status;
    }

    public datas getData() {
        return data;
    }

    public static class datas {
        @SerializedName("device_id")
        private String device_id;

        @SerializedName("device_name")
        private String device_name;

        @SerializedName("app_version")
        private String app_version;

        @SerializedName("android_version")
        private String android_version;

        @SerializedName("feedback")
        private String feedback;

        public String getDevice_id() {
            return device_id;
        }

        public String getDevice_name() {
            return device_name;
        }

        public String getApp_version() {
            return app_version;
        }

        public String getAndroid_version() {
            return android_version;
        }

        public String getFeedback() {
            return feedback;
        }
    }
}
